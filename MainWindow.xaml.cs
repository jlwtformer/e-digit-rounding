﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace E_Digits
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		const double the_e = Math.E;
		int e_nums;

		public MainWindow()
		{
			InitializeComponent();
		}

		public void btnGenerate_Click(object sender, RoutedEventArgs e)
		{
			e_nums = Convert.ToInt32(txtBoxNum.Text);

			if (IsProperValue(e_nums) == true)
			{
				txtResults.Text = Convert.ToString(Math.Round(the_e, e_nums));
			}
			else
			{
				txtResults.Text = "Please enter a number lower than 15.";
			}
		}

		public static bool IsProperValue(int value)
		{
			if (value >= 15)
			{
				return false;
			}

			return true;
		}
	}
}
